/*****************************************************  Bonus бодлогууд **********************************************************************/

/*****************************************************  ID: 14143 "Хөөрхөн матриц"   *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int n[5][5];
    int posx, posy;
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            cin >> n[i][j];
            if (n[i][j] == 1) {
                posx = i+1;
                posy = j+1;
            }
        }
    }
    posx = abs(3-posx);
    posy = abs(3-posy);
    cout << posx + posy << endl;
    return 0;
} 
/*****************************************************  ID: 13663 "Утга олголт 0"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int m, c=0;
    cin >> m;
    int n[m][m];
    for (int i = 1; i <= m*m; i++) {
          if (i < 10){
               cout << "  " << i << " ";
               c++;
          } else{
               cout << " "<<i << " ";
               c++;
          }
          if (c == m){
               cout << endl;
                    c=0;
          }   
     }
     return 0;
} 

/*****************************************************  ID: 13664 "Утга олголт 1"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int m,c=1, d=1;
    cin >> m;
    int n[m][m];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            n[i][j] = c;
            c++;
        }
    }
    for (int i = 0; i < m; i++) {
        if (i%2 == 0) {            
            for (int j = 0; j < m; j++) {
                if (n[i][j] < 10) {
                    cout << "  " << n[i][j];
                }
                else{
                      cout << " " << n[i][j];
                }
            }
        }
        else { 
        for (int j = m-1; j >= 0; j--) {
            if (n[i][j] < 10) {
                cout << "  " << n[i][j];
            }
            else {
                  cout << " " << n[i][j];
            }
        }
        }
        cout << endl; 
        
    }
    return 0;
} 
/*****************************************************  ID: 13665 "Утга олголт 2"  *********************************************************/

#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int m,c=1, d=1;
    cin >> m;
    int n[m][m];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            n[i][j] = c;
            c++;
        }
    }
    for (int i = 0; i < m; i++) {
        
        for (int j = m-1; j >= 0; j--) {
            if (n[i][j] < 10) {
                cout << "  " << n[i][j];
            }
            else {
                  cout << " " << n[i][j];
            }
        }
        
        cout << endl; 
        
    }
    return 0;
} 

/*****************************************************  ID: 13666 "Утга олголт 3"  *********************************************************/

#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
     int m,c=1, d=1;
     cin >> m;
     int n[m][m];
     for (int i = 0; i < m; i++) {
          for (int j = 0; j < m; j++) {
               n[i][j] = c;
               c++;
          }
     }
     for (int i = 0; i < m; i++) {
          if (i%2 == 1) {            
               for (int j = 0; j < m; j++) {
                    if (n[i][j] < 10) {
                         cout << "  " << n[i][j];
                    } else{
                         cout << " " << n[i][j];
                    }
               }
          } else { 
               for (int j = m-1; j >= 0; j--) {
                    if (n[i][j] < 10) {
                         cout << "  " << n[i][j];
                    } else {
                         cout << " " << n[i][j];
                    }
               }
          }
          cout << endl; 
          
     }
     return 0;
} 

/*****************************************************  ID: 13667 "Утга олголт 4"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
     int m,c=1, d=1;
     cin >> m;
     int n[m][m];
     for (int i = 0; i < m; i++) {
          for (int j = 0; j < m; j++) {
               n[i][j] = c;
               c++;
          }
     }
     for (int i = m-1; i >= 0; i--) {
          for (int j = m-1; j >= 0; j--) {
               if (n[i][j] < 10) {
                    cout << "  " << n[i][j];
               }
               else{
                    cout << " " << n[i][j];
               }   
          }
          cout << endl; 
     }
     return 0;
} 
/*****************************************************  ID: 13668 "Утга олголт 5"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int m,c=1, d=0;
    cin >> m;
    int n[m][m];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            n[i][j] = c;
            c++;
        }
    }
    for (int i = m-1; i >= 0; i--) {
        if (d%2 == 0) {            
            for (int j = m-1; j >= 0; j--) {
                if (n[i][j] < 10) {
                     cout << "  " << n[i][j];
                } else{
                    cout << " " << n[i][j];
                }
            }
        } else { 
            for (int j = 0; j < m; j++) {
                if (n[i][j] < 10) {
                    cout << "  " << n[i][j];
                } else {
                     cout << " " << n[i][j];
                }
            }
        }
        d++;
        cout << endl; 
    }
    return 0;
} 
/*****************************************************  ID: 13670 "Утга олголт 6"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int m,c=1, d=0;
    cin >> m;
    int n[m][m];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            n[i][j] = c;
            c++;
        }
    }
    for (int i = m-1; i >= 0; i--) {
        if (d%2 == 1) {            
            for (int j = 0; j < m; j++) {
                if (n[i][j] < 10) {
                     cout << "  " << n[i][j];
                } else{
                    cout << " " << n[i][j];
                }
            }
        } else { 
            for (int j = 0; j < m ; j++) {
                if (n[i][j] < 10) {
                    cout << "  " << n[i][j];
                } else {
                     cout << " " << n[i][j];
                }
            }
        }
        d++;
        cout << endl; 
    }
    return 0;
} 
/*****************************************************  ID: 13671 "Утга олголт 7"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
     int m,c=1, d=0;
     cin >> m;
     int n[m][m];
     for (int i = 0; i < m; i++) {
          for (int j = 0; j < m; j++) {
               n[i][j] = c;
               c++;
          }
     }
     for (int i = m-1; i >= 0; i--) {
          if (d%2 == 1) {            
               for (int j = m-1; j >= 0; j--) {
                    if (n[i][j] < 10) {
                         cout << "  " << n[i][j];
                    } else{
                         cout << " " << n[i][j];
                    }
               }
          } else { 
               for (int j = 0; j < m ; j++) {
                    if (n[i][j] < 10) {
                         cout << "  " << n[i][j];
                    } else {
                         cout << " " << n[i][j];
                    }
               }
          }
          d++;
          cout << endl; 
     }
    return 0;
} 
/*****************************************************  ID: 14070 "Массив нийлбэр"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    
     int n,m;
     cin >> n >> m ;
     int a[n][m], s=0;
     for (int i = 0; i < n; i++) {
          for (int j = 0; j < m; j++) {
               cin >> a[i][j];
               s +=a[i][j];
          }
     }
     cout << s << endl;
     return 0;
} 
/*****************************************************  ID: 14071 "Массивын ихийн байрлал"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    
    int n,m;
    cin >> n >> m;
    int a[n][m], max = -99999, x,y;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> a[i][j];
            if (max < a[i][j])
            {
                max = a[i][j];
                x = i+1;
                y = j+1;
            }   
        }
    }
    cout << max << " "  << x << " " <<y << endl;
    return 0;
} 

/*****************************************************  ID: 14072 "Хамгийн бага нь хэд"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int n,m, c=0;
    cin >> n >> m;
    int a[n][m], min = 9999999, x,y;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> a[i][j];
            if (min > a[i][j])
            {
                min = a[i][j];
            }   
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (a[i][j] == min)
            {
                c++;
            }
        }
    }
    cout << min <<" "<<c<< endl;
    return 0;
} 
/*****************************************************  ID: 14073 "Мөрүүдийн ихүүд"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int n,m, c=0;
    cin >> n >> m;
    int a[n][m], x,y, b[n];
    for (int i = 0; i < n; i++) {
        b[i] = -999999;
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> a[i][j];
            if (b[i] < a[i][j]) b[i] = a[i][j];
        }
    }
    for (int i = 0; i < n; i++) {
        cout<< b[i]<<endl;
    }
    return 0;
} 
/*****************************************************  ID: 14074 "Багануудын багууд"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
     int n,m, c=0;
     cin >> n >> m;
     int a[n][m], x,y, b[m];
     for (int i = 0; i < m; i++) {
          b[i] = 999999;
     }
     for (int i = 0; i < n; i++) {
          for (int j = 0; j < m; j++) {
               cin >> a[i][j];
          }
     }
     int a1[m][n];
     for (int i = 0; i < max(n,m); i++) {
          for (int j = 0; j < max(n,m); j++) {
               a1[i][j] = a[j][i];
          }
     }
     for (int i = 0; i < m; i++) {
          for (int j = 0; j <n; j++) {
               if (b[i] > a1[i][j]) b[i] = a1[i][j];
          }
     }
     for (int i = 0; i < m; i++) {
          cout<< b[i]<<" ";
     }
     cout<<endl;
     return 0;
} 
/*****************************************************  ID: 14136 "Массив эргүүлэлт"  *********************************************************/
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main()
{
    int n,m;
    cin >> n >> m;
    int a[n][m];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> a[i][j];
        }
    }
    int a1[m][n];
    for (int i = 0; i < max(n,m); i++) {
        for (int j = 0; j < max(n,m); j++) {
           a1[i][j] = a[j][i];
        }
    }
    cout<< m<<" "<<n<<endl;
    for (int i = 0; i < m; i++) {
        for (int j = n-1; j >= 0; j--) {
            cout<<a1[i][j]<< " ";
        }
        cout << endl;
    }
    return 0;
} 